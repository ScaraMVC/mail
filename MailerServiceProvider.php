<?php

namespace Scara\Mail;

use Scara\Config\Configuration;
use Scara\Support\ServiceProvider;

/**
 * Mailer Service Provider.
 */
class MailerServiceProvider extends ServiceProvider
{
    /**
     * Registers the Mailer Service Provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('mail', function () {
            // return new Mailer();
            $c = new Configuration();
            $config = $c->from('mail');

            $mail = new Mailer($config->get('host'), $config->get('port'),
                $config->get('smtp_auth'), $config->get('encryption'));

            return $mail->setAuthentication($config->get('user'), $config->get('pass'));
        });
    }
}
