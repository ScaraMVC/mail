<?php

namespace Scara\Mail;

/**
 * Custom Mailer Exception.
 */
class MailerException extends \Exception
{
}
